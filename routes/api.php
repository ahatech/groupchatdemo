<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('group')->middleware('owner')->group(function(){
    Route::get('/create', 'GroupController@create_form');
    Route::post('/create', 'GroupController@create');            
    Route::get('/join', 'GroupController@join_form');            
    Route::post('/join', 'GroupController@join');            
    Route::get('/{id}', 'GroupController@show_group');            
    Route::get('/edit/{id}', 'GroupController@edit');            
    Route::post('/update/{id}', 'GroupController@update');            
    Route::delete('/delete/{id}', 'GroupController@delete');            
    Route::get('/members_list/{id}', 'GroupController@members_list');
});
Route::post('/send_message/{id}', 'MessageController@send_message');
Route::get('/show_messages/{id}', 'MessageController@show_messages');