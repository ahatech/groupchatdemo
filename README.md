# Group Chat Application in Laravel

This is for client Demo.

After cloning the project

1. Rename ```.env.example``` file to ```.env```

For real time features, go to https://pusher.com/ create an account and put your pusher credentials in the ```.env``` file

2. Put your database details in ```.env``` file

3. Type ```composer update``` to install the composer packages

4. Type ```npm install``` or ```npm update``` to install node modules

5. Type ```php artisan migrate``` to migrate the tables

6. ```php artisan serve``` to start using the app.

7. Finally ```npm run dev``` to start Vue Js development mode