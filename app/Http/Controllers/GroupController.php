<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Group;

class GroupController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');

        //some functions can only be executed by group admin/owner
        $this->middleware('owner')->only(['edit', 'update', 'delete', 'remove_user']);

        //the group will only be accessed by a member
        $this->middleware('member')->only('show');
    }

    //display form to create a group
    public function create_form()
    {
        return view('group.create');
    }

    public function create(Request $request)
    {
        $this->validate($request, [
            'name' => 'required'
        ]);

        //generate a code for the groupe
        $characters = 'abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $code = substr(str_shuffle($characters), rand(0, 9), 7);

        $group = Group::create([
            'name' => $request->name,
            'code' => $code,
            'admin_id' => auth()->user()->id,
        ]);
        
        //we attach the user with the group after he created it
       $group->participants()->attach(auth()->user()->id);

       return response()->json([
        'status' => true,
        'message' => 'Your group has been created',
       ]);
    }

    //display the form to join a group
    public function join_form()
    {
        return view('group.join');
    }

    //user join a group by entering the code
    public function join(Request $request)
    {
        $this->validate($request, [
            'code' => 'required'
        ]);

        $code = $request->code;

        $group = Group::where('code', $code)->first();

        //if the group exists
        if ($group)
        {
            try 
            {
                //we add the user to the group and we redirect him to the home page with a success message

                $group->participants()->attach(auth()->user()->id);
                return response()->json([
                    'status' => true,
                    'message' => 'Group joined',
                ]);
            } 
            catch (\Throwable $th) 
            {
                //Display an error if the user is already in the group
                return response()->json([
                    'status' => false,
                    'message' => 'You are already a member of this group',
                ]);
            }
        }
        else
        {
            //if the group doesn't exist we throw an error
            return response()->json([
                'status' => false,
                'message' => 'Group not found',
            ]);
        }
    }

    //show group page with messages
    public function show_group($id)
    {
        $group = Group::find($id);
        $messages = $group->messages()->get();
        $data['group'] = $group;
        $data['messages'] = $messages;

        return response()->json([
            'status' => true,
            'data' => $data,
            'message' => 'Group details',
        ]);
    }

    //display the form to edit the name of a group
    public function edit($id)
    {
        $group = Group::find($id);
        $data['group'] = $group;
        if ($group) {
            return response()->json([
                'status' => true,
                'data'=> $data,
                'message' => 'Group data',
            ]);
        }
        return response()->json([
            'status' => false,
            'message' => 'Error, try again',
        ]);
    }

    //change the name of the group
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required'
        ]);
        $group = Group::find($id);
        $group->name = $request->name;
        if ($group->save()) {
            return response()->json([
                'status' => true,
                'message' => 'Group name has been changed',
            ]);
        }
        return response()->json([
            'status' => false,
            'message' => 'Error, try again',
        ]);
    }

    //delete a groupe and remove every participant
    public function delete($id)
    {
        $group = Group::find($id);
        $group->participants()->detach();
        $group->delete();
        if ($group->delete()) {
            return response()->json([
                'status' => true,
                'message' => 'Group has been deleted',
            ]);
        }
        return response()->json([
            'status' => false,
            'message' => 'Error, try again',
        ]);
    }

    //display the members of a group, the admin can then decide who to remove
    public function members_list($id)
    {
        $group = Group::find($id);
        $group_members = $group->participants()->get();
        $group_id = $id;
        return response()->json([
            'status' => true,
            'message' => 'A user has been removed',
        ]);
    }

    //remove the user from a group
    public function remove_user($id, $user_id)
    {
        $group = Group::find($id);
        $group->participants()->detach($id);
        return response()->json([
            'status' => true,
            'message' => 'A user has been removed',
        ]);
    }
}
